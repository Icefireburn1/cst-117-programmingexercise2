﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ShapeMaker
{
    public partial class Form1 : Form
    {

        string baseDirectory = "";
        string imageLocation = "";

        public Form1()
        {
            InitializeComponent();

            baseDirectory = System.IO.Path.GetDirectoryName(Application.ExecutablePath);

            listBox1.Items.Add("Circle");
            listBox1.Items.Add("Triangle");
            listBox1.Items.Add("Rectangle");
            listBox1.Items.Add("Pentagon");
            listBox1.Items.Add("Hexagon");

            listBox1.SetSelected(0, true);
        }

        private void drawButton_Click(object sender, EventArgs e)
        {
            switch (listBox1.SelectedItem.ToString())
            {
                case "Circle":
                    if (fillRadioButton.Checked)
                        imageLocation = baseDirectory + @"\Pictures\circle_filled.png";
                    else
                        imageLocation = baseDirectory + @"\Pictures\circle_outline.png";
                    break;

                case "Triangle":
                    if (fillRadioButton.Checked)
                        imageLocation = baseDirectory + @"\Pictures\triangle_filled.png";
                    else
                        imageLocation = baseDirectory + @"\Pictures\triangle_outline.png";
                    break;

                case "Rectangle":
                    if (fillRadioButton.Checked)
                        imageLocation = baseDirectory + @"\Pictures\rectangle_filled.png";
                    else
                        imageLocation = baseDirectory + @"\Pictures\rectangle_outline.png";
                    break;

                case "Pentagon":
                    if (fillRadioButton.Checked)
                        imageLocation = baseDirectory + @"\Pictures\pentagon_filled.png";
                    else
                        imageLocation = baseDirectory + @"\Pictures\pentagon_outline.png";
                    break;

                case "Hexagon":
                    if (fillRadioButton.Checked)
                        imageLocation = baseDirectory + @"\Pictures\hexagon_filled.png";
                    else
                        imageLocation = baseDirectory + @"\Pictures\hexagon_outline.png";
                    break;
            }

            if (dateCheckBox.Checked)
            {
                dateLabel.Text = DateTime.Now.ToString("M/d/yyyy");
                dateLabel.Visible = true;
            }
            else
            {
                dateLabel.Visible = false;
            }

            if (nameCheckbox.Checked)
            {
                nameLabel.Text = listBox1.SelectedItem.ToString();
                nameLabel.Visible = true;
            }
            else
            {
                nameLabel.Visible = false;
            }
                

            pictureBox1.ImageLocation = imageLocation;
        }

        private void radioButton1_CheckedChanged(object sender, EventArgs e)
        {

        }

        private void listBox1_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        private void clearButton_Click(object sender, EventArgs e)
        {
            pictureBox1.Image = null;
            nameLabel.Visible = false;
            dateLabel.Visible = false;
        }
    }
}
